---
layout: page
title: Impressum
permalink: /impressum/
nav_order: 1000
---

![Website-Banner](/assets/website_banner.png)
# Impressum

Kontakt zur verantwortlichen Person:
<br><br>
Manuel Bühlmaier

Hauptstraße 10

86453 Dasing

E-Mail:
[redbacon22@hotmail.com](mailto:redbacon22@hotmail.com)
