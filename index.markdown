---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
title: Aktuelles
nav_order: 1
---

# Willkommen bei Augsburg Solidarisch!
![Website-Banner](/assets/website_banner.png)

## Aktuelles

### 6. März 2022
<br>
<b>Fahnenkampagne! (beendet)</b>

<div style="display: grid; grid-template-columns: 300px 100%; grid-column-gap: 20px">
<div>
<p>
Wir haben eine Kampagne gestartet und eine Fahne designed, mit der ihr eure Unterstützung für unsere Themen in der Öffentlichkeit zeigen könnt.
Diese Fahne könnte auf Aktionen zum Einsatz kommen oder zum Beispiel aus dem Fenster der Wohnung gehängt werden.
<a href="https://startnext.de/augsburgsolidarisch">Hier</a> ist der Link zu unserem Crowdfounding-Projekt,
 wo Du die Fahne "Geradeaus denken - solidarisch handeln" zum Preis von 10 Euro erwerben kannst.
Beachte dabei bitte, dass wir die Fahnen erst produzieren lassen können, wenn der Zeitablauf des Projekts abgeschlossen ist, die Produktion wird ca. 10 Tage dauern.
Alle Besteller*innen werden darüber nochmal ausführlich informiert. Und jetzt, ein <a href="https://startnext.de/augsburgsolidarisch">*klick*</a>
 für mehr Solidarität und faires Miteinander. Dankeschön!
</p>
</div>

<div>
<a href="https://startnext.com/augsburgsolidarisch">
<img style="width: 30%; aspect-ratio: attr(width) / attr(height)" src="/assets/fahne.jpg"></a>
</div>
</div>

### 6. März 2022, 12 Uhr

<b>Alle Zusammen für ein solidarisches Augsburg</b>

<img style="height: 400px; aspect-ratio: attr(width) / attr(height)" src="/assets/sharepic_06_03.jpg">


Die Pandemie zerrt an den Nerven, 
die Klimakatastrophe wird immer präsenter und in der Ukraine wird geschossen. 

Die Welt steht still.
Doch die Menschen in der Ukraine leiden unter den Kriegshandlungen Putins und verdienen Hilfe und Solidarität. Aus aller Welt!
Deshalb haben wir uns entschieden, in einer Menschenkette durch Augsburg zu laufen! Denn was fehlt und unbeachtet bleibt, sind Menschenwürde und Menschenrechte. Diese verbindet uns in den Krisen unserer Zeit miteinander.

Um uns solidarisch mit den Menschen in der Ukraine zu zeigen und um rechtem und querdenkenden Gedankengut entgegenzuwirken, starten wir am Sonntag, den 6.3 eine Menschenkette durch die Innenstadt Augsburg. Mit Fahnen werden wir die Innenstadt in einen bewegten Solidaritätszug verwandeln und für ein solidarisches Augsburg einstehen.

Flagge zeigen für Menschenrechte!

\#augsburgsolidarisch \#menschenkette \#geradeausdenken \#solidarischhandeln \#solidarität \#westandwithukraine \#people \#augsburg \#rathausplatz \#stopwar

<iframe width=600 height=337 src="https://www.youtube.com/embed/yrRbntM1tyY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
