---
layout: page
title: Über uns
permalink: /about/
nav_order: 10
has_children: true
has_toc: false
---

![Website-Banner](/assets/website_banner.png)

# Über uns
Augsburg Solidarisch ist ein Bündnis von Menschen, die sich für einen **wissenschaftsbasierten** und **solidarischen** Weg durch die Pandemie einsetzen. Dabei verstehen wir uns bewusst nicht nur
als Gegenbewegung zu Querdenken o.Ä., sondern als Initiative, die selbst Ideen und Forderungen an die Politik äußert. Unser Selbstverständnis könnt ihr nachlesen unter: [https://augsburg-solidarisch.de/selbstverstaendnis](/selbstverstaendnis).
