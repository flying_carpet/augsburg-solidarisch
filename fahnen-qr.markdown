---
layout: page
permalink: /s/fahnen/
---

# Fahnenkampagne

Du bist dem QR Code gefolgt und auf unserer Homepage gelandet.
[Hier](https://startnext.de/augsburgsolidarisch) ist der Link zu unserem Crowdfounding-Projekt,
wo Du die Fahne "Geradeaus denken - solidarisch handeln" zum Preis von 10 Euro erwerben kannst.
Beachte dabei bitte, dass wir die Fahnen erst produzieren lassen können, wenn der Zeitablauf des Projekts abgeschlossen ist, die Produktion wird ca. 10 Tage dauern.
Alle Besteller\*innen werden darüber nochmal ausführlich informiert. Und jetzt, ein [\*klick\*](https://startnext.de/augsburgsolidarisch)
für mehr Solidarität und faires Miteinander. Dankeschön!

<a href="https://startnext.com/augsburgsolidarisch"><img align="middle" style="height: 500px; aspect-ratio: attr(width) / attr(height); display: block; margin-left: auto; margin-right: auto" src="/assets/fahne.jpg"></a>
