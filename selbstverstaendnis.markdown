---
layout: page
title: Selbstverständnis
permalink: /selbstverstaendnis/
parent: "Über uns"
nav_order: 1
---

![Website-Banner](/assets/website_banner.png)

# Selbstverständnis

Augsburg Solidarisch versteht sich als ein Bündnis von Gruppierungen, das sich für einen solidarischen und wissenschaftsbasierten Weg aus der Pandemie einsetzt. Im Rahmen der Pandemie hat sich die Situation vieler Menschen verschlechtert. Kurzarbeit, Sinken der Reallöhne, steigende Bildungsungerichtigkeit, unsolidarische Impfstoffverteilung, Ignorieren der Interessen von benachteiligten Gruppen und Infektionsschutz gehören zu den Themen, die wir bearbeiten möchten.

## Ausrichtung/Auftreten

Während unserer Aktionen werden wir auch Bezug auf die Corona-Gegner\*innen nehmen, da sie die Gesundheit von uns allen riskieren und bereitwillig mit Rechtsextremen zusammenarbeiten - diese Akzeptanz von rechten und rechtsextremen Akteur\*innen innerhalb der Querdenkenden stellt eine Gefahr für die gesamte Gesellschaft dar. Wir sind jedoch nicht die bloße Gegenreaktion auf die Querdenkenden. Wir verstehen uns als eine Bewegung, die einen Weg aus der Krise in einem wissenschaftsorientierten Diskurs unter Berücksichtigung der Interessen insbesondere von systemisch benachteiligten Individuen und Gruppen sieht und dabei nicht von reaktionären Akteur\*innen abhängig ist. 

## Systemkritik

Dabei gehen wir kritisch aber konstruktiv mit der Landes- und Bundesregierung um. Neben den Spezifika einzelner Maßnahmen, richten wir unseren Blick auf eine systemische Analyse. Wir benennen klar, dass es in der Krisenbewältigung bisher Gewinnende und Verlierende gab und fordern eine Krisenbewältigung, die für uns alle funktioniert. Diese kann nur antikapitalistisch, antirassistisch und queer-feministisch gestaltet werden. Natürlich darf es Kritik an der Regierung geben. Doch die Basis der Kritik darf kein blinder Egoismus sein. Die selbe Solidarität, die die Augsburger\*innen dazu veranlasst hat die Einschränkungen in der Corona-Pandemie hinzunehmen, sollte auch grundlegend für eine Kritik der Maßnahmen sein. 

## Zielgruppe

Mit unseren Veranstaltungen richten wir uns also gezielt an die gesamte Gesellschaft. An Menschen, die die unsolidarischen und egoistischen Botschaften der Maßnahmen-Skeptiker\*innen nicht länger unwidersprochen lassen möchten. Außerdem richten wir uns an Menschen die eine differenzierte Kritik an den Maßnahmen der Regierung und kapitalistischen Akteur\*innen auf der Basis einer gesamtgesellschaftlichen Solidarität üben wollen.
