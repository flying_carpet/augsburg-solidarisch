---
layout: page
title: Kontakt
permalink: /kontakt/
nav_order: 990
---

![Website-Banner](/assets/website_banner.png)

# Kontakt

Hast du eine Frage? Möchtest du gerne mitmachen? Willst du wissen, was gerade für Termine anstehen? Infos kannst du auf folgenden Kanälen enthalten:

## Telegram

Infochannel: [https://t.me/augsburgsolidarisch](https://t.me/augsburgsolidarisch)
Austauschgruppe: [https://t.me/+wEJpju4L0XM0NWEy](https://t.me/+wEJpju4L0XM0NWEy)

## Instagram

[https://www.instagram.com/augsburgsolidarisch/](https://www.instagram.com/augsburgsolidarisch)

## Facebook

[https://www.facebook.com/auxsolidarisch/](https://www.facebook.com/auxsolidarisch/)

## Twitter

[https://twitter.com/Aux_Solidarisch](https://twitter.com/Aux_Solidarisch)

## E-Mail

[augsburgsolidarisch@riseup.net](mailto:augsburgsolidarisch@riseup.net)
