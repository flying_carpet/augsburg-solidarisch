---
layout: page
title: Mitmachen und helfen
permalink: /mitmachen/
nav_order: 20
---

![Website-Banner](/assets/website_banner.png)

# Mitmachen und helfen!

Dir liegt unser Themengebiet genauso am Herzen wie uns? Dann gibt es verschiedene Möglichkeiten, wie du aktiv werden und dich beteiligen kannst:

## Demonstrationen und Kundgebungen besuchen

Über unsere Website sowie die Social-Media-Profile (zu finden unter [Kontakt](/kontakt) und unseren Telegram-Kanal ([https://t.me/augsburgsolidarisch](https://t.me/augsburgsolidarisch)) bleibst du immer auf dem Laufenden und erfährst, wann wir unsere Demonstrationen, Kundgebungen und Aktionen veranstalten.

## Ins Bündnis einsteigen

Wenn du dich über unsere großen Kundgebungen hinaus für einen solidarischen Weg durch die Pandemie und ein Handeln auf Basis von wissenschaftlichen Fakten engagieren willst, kannst du in unser Bündnis einsteigen. Du kannst uns dafür entweder direkt auf einer unserer Aktionen ansprechen oder zu uns [Kontakt](/kontakt) aufnehmen, z.B. per Email unter [augsburgsolidarisch@riseup.net](mailto:augsburgsolidarisch@riseup.net)
